# Level II indicator for NinjaTrader 7
#### Displays orderbook/marketdepth right on chart.

## Downloads
You can get *newest version* and all *earlier releases* of this indicator in the **[download section](//bitbucket.org/ninjatrader/level2/downloads)**.

![a picture is worth a thousand words](//bytebucket.org/ninjatrader/level2/wiki/img/dom-lvl2.png)

## Features
+ Resource efficient
+ Adjustable font
+ Cramped mode
+ Volume sums
+ Maximal volume
+ Adjustable max./min. bar width/height

## Step by Step
1. download Indicator
1. import Indicator
1. configure Indicator

## Contact
You've found a bug, have suggestions or feedback? **[Create a Ticket!](https://bitbucket.org/ninjatrader/level2/issues/new)**

## Links
+ [BMT Download and Reviews](https://www.bigmiketrading.com/download/ninjatrader-7/indicators/1590-download.html)
+ [ninjatrader.com > Version 7 Indicators](http://www.ninjatrader.com/support/forum/local_links.php?catid=4&linkid=640)
+ [Source Code](//bitbucket.org/ninjatrader/level2/src/master/Indicator/VitLevel2.cs) ([Raw](//bitbucket.org/ninjatrader/level2/raw/master/Indicator/VitLevel2.cs))
+ [Changelog](//bitbucket.org/ninjatrader/level2/src/master/CHANGELOG.md)
+ [Commit History](//bitbucket.org/ninjatrader/level2/commits)
+ [Other Indicators](//bitbucket.org/ninjatrader)

## See also
+ EdsLevel2
+ jtRealStats
