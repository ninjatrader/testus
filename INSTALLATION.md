# Compilation and Installation

## Step 1
Open indicator repertory.

![Step 1](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_compile_step1.png)


## Step 2
Doubleclick the indicator to open it in a new window.

![Step 2](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_compile_step2.png)


## Step 3
Click F5 to compile the indicator. You should hear a bell on success.
![Step 3](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_compile_step3.png)


Congratulation! You can use your new level II indicator!!

![Level 2 Indicator](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_indi.png)
